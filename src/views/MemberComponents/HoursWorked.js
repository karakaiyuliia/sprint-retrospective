import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import theme from "../theme";
import ComboSelect from "react-combo-select";
import "../../App.css";
require("../../../node_modules/react-combo-select/style.css");
let stories = []; // stories JSONs to be passed by parent

class HoursWorked extends React.PureComponent {
  state = {
    selectedMsg: "",
    stories: [], // to be passed from parent component
    completedStories: [],
    NumberOfHours: [],
    members: [], // to populated with a fetch from the database
    membername: "",
    estimationMsg: "nothing"
  };

  sendParentSomeData = msg => {
    this.props.dataFromChild(msg);
  };

  async componentDidMount() {
    stories = this.props.allStories;
    let membTemp = [];
    let completedStories = [];

    stories.map(s => {
      if (s.isDone) completedStories.push(s);
    });

    this.setState({
      selectedMsg: "",
      userInfo: "",
      stories: this.props.allStories,
      completedStories: completedStories
    });

    this.props.allSprints.map(s => {
      if (s.isDone) membTemp.push(s.sprint);
    });

    this.setState({
      members: membTemp
    });
  }

  async onSprintChoosen(value, text) {
    // Fetch all user stories in that sprint.
    let allUserStories = [];

    stories.map(s => {
      if (s.sprint === value) allUserStories.push(s);
    });

    this.setState({
      completedStories: allUserStories
    });
  }

  async onMemberChoosen(value, text) {
    let hours = [];

    var i;
    for (i = 0; i < value.developers.length; i++) {
      hours.push({
        user: value.developers[i],
        hours: value.hoursWorked[i],
        story: value.name
      });
    }

    this.setState({
      NumberOfHours: hours
    });
  }

  render() {
    const { members, completedStories } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <Typography
            style={{ textAlign: "center" }}
            variant="h4"
            color="primary"
          >
            Calculate number of hours worked for each user story
          </Typography>
        </div>

        <Card style={{ marginTop: "10%" }}>
          <CardHeader />

          <CardContent style={{ marginTop: "10%" }}>
            {/* Display ComboSelect containing members to choose form - using member array  */}
            {/* When user select a member, calculate estimate accuracy using stories JSON array */}
            <ComboSelect
              data={members}
              map={{ text: "sprint", value: "sprint" }}
              text="-Select a completed sprint-"
              onChange={this.onSprintChoosen.bind(this)}
            />

            <ComboSelect
              data={completedStories}
              map={{ text: "name", value: true }}
              text="-Select a story-"
              onChange={this.onMemberChoosen.bind(this)}
            />

            {/** Display the estimate as plain text */}
            <h3>Display number of hours worked</h3>
            {this.state.NumberOfHours.map(s => (
              <div>
                {s.user} worked {s.hours} hours in this user story
              </div>
            ))}
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}

export default HoursWorked;
