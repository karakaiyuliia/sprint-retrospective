import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import {
    Card,
    CardHeader,
    CardContent,
    IconButton,
    TextField
} from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircleRounded';
import SendIcon from '@material-ui/icons/Send';
import theme from './theme';
import '../Login.scss';
import regController from '../controllers/Register';


class Register extends React.Component {

    state = {
        email: "",
        password: "",
        errors: [],
    };

    handleEmailInput = e => {
        this.setState({ email: e.target.value });
    };

    handlePasswordInput = e => {
        this.setState({ password: e.target.value });
    };

    onRegisterClicked = () =>{
        regController.registerUser(this.state.email, this.state.password);
    };

    render() {
        const { email, password } = this.state;
        const emptyorundefined =
            email === undefined ||
            email === '' ||
            password === undefined ||
            password === '';

        return (

            <MuiThemeProvider theme={theme}>
                <Card style={{ marginTop: '0', width:'350px' }}>
                    <CardHeader
                        title="Enter email and password to sign up"
                        color="inherit"
                        style={{ textAlign: 'center' }}
                    />
                    <CardContent>
                        <TextField
                            style={{width: '300px'}}
                            onChange={this.handleEmailInput}
                            placeholder="Email"
                        />
                        <br />
                        <br />
                        <TextField
                            style={{width: '300px'}}
                            type="password"
                            onChange={this.handlePasswordInput}
                            placeholder="Password"
                        />
                        <IconButton
                            color="primary"
                            style={{ marginTop: 50, float: 'right' }}
                            onClick={this.onRegisterClicked}
                            disabled={emptyorundefined}
                        >
                            <AddCircle fontSize="large" component={SendIcon}/>
                        </IconButton>
                    </CardContent>
                </Card>
            </MuiThemeProvider>
        );
    }
}

export default Register;