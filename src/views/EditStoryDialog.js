import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import controller from "../controllers/Project"


export default class EditStory extends React.Component {
    state = {
        open: false,
        storyName: this.props.story,
        sprint: this.props.sprint,
        worked: this.props.hoursWorked,
        estimate: this.props.Reestimate,
        storyId: '',
        storyOwner: this.props.owner,
        storyStart: this.props.start,
        storyEnd: this.props.end,
        storyIsDone: this.props.isDone,
        storyProject: this.props.project,
        storyDevelopers: this.props.developers
    };

    async componentDidMount() {
        this.setState({open: this.props.open});
        this.setState({storyId: await controller.getId("stories", "name", this.props.story)});
    };

    sendParentSomeData = () => {
        this.props.openFromDialog(false);

    };

    handleSprintChange = e => {
        this.setState({ sprint: e.target.value });

    };

    handleWorkedChange = e => {
        this.setState({ worked: e.target.value });

    };

    handleEstimateChange = e => {
        this.setState({ estimate: e.target.value });

    };

    handleClose = () => {
        this.setState({ open: false });

        this.sendParentSomeData();
    };

    handleSave = () => {
        if(this.state.storyId !== null){
            controller.updateStory(this.state.storyId[0], this.state.sprint, this.state.worked, this.state.estimate, this.state.storyOwner, this.state.storyName, this.state.storyStart, this.state.storyEnd, this.state.storyIsDone, this.state.storyProject, this.state.storyDevelopers);
            this.handleClose();

        }
    };

    render() {
        return (
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add new project</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Hours worked"
                            type="number"
                            defaultValue={this.state.worked}
                            onChange={this.handleWorkedChange}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="start"
                            label="Reestimate"
                            type="number"
                            defaultValue={this.state.estimate}
                            onChange={this.handleEstimateChange}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="sprint"
                            label="Sprint"
                            type="number"
                            defaultValue={this.state.sprint}
                            onChange={this.handleSprintChange}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSave} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}