import React from 'react';
import {MuiThemeProvider} from '@material-ui/core/styles';
import {
    Card,
    CardContent,
} from '@material-ui/core';
import AddCircle from '@material-ui/icons/AddCircleRounded';
import theme from './theme';
import Typography from "@material-ui/core/Typography";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardActions from "@material-ui/core/CardActions";
import projectLogo from '../supporting/project1.png';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Project from "./ProjectComponent"
import IconButton from "@material-ui/core/IconButton";
import AddProject from "./AddProjectDialog";

let projectToLoad;
let selectedProjStories = [];

class ProjectList extends React.Component {

    state = {
        projects: [],
        loadProject: this.props.loadProject,
        addProject: false,
        stories: [],
        users: this.props.users
    };

    onAddClicked = () => {
        this.setState({addProject: true})
    };

    msgFromChild = (msg) => {
        this.setState({addProject: msg});
        //this.setState({projects: await controller.getProjects()});

        // console.log(this.state.projects);
        // this.forceUpdate();
    };

    onViewClicked = (name) => {
        selectedProjStories = [];
        projectToLoad = name;

        for(let key in this.state.stories) {
            if(this.state.stories[key].project === name)
                selectedProjStories.push(this.state.stories[key]);
        }

        this.setState({loadProject: true});
    };

    async componentDidMount() {
        this.setState({projects: this.props.projects});
        this.setState({stories: this.props.stories});
    };

    render() {
        return (
            <MuiThemeProvider theme={theme}>
                {this.state.loadProject ? null : <IconButton
                    color="secondary"
                    style={{marginTop: 5, float: 'right'}}
                    onClick={this.onAddClicked}
                >
                    <AddCircle fontSize="large"/>
                </IconButton>}
                {this.state.loadProject ? null : <Grid container spacing={16} style={{
                    flexGrow: 1,
                    marginTop: '3%'
                }}>
                    <Grid item xs={12} style={{padding: '25px'}}>
                        <Grid container justify="center" spacing={16}>
                            {this.state.projects.map((prj, index) => (
                                <Grid key={index} item>
                                    <Card style={{maxWidth: '350px', minWidth: '300px', alignContent: 'center'}}
                                          key={index}>
                                        <CardActionArea>
                                            <CardMedia
                                                style={{height: '140px'}}
                                                image={projectLogo}
                                                title="project"
                                            />
                                            <CardContent>
                                                <Typography gutterBottom variant="h5" component="h2">
                                                    {prj.name}
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button size="small" color="primary"
                                                    onClick={() => this.onViewClicked(prj.name)}>
                                                View
                                            </Button>
                                            {!prj.finished ?
                                            <Button size="small" color="primary">
                                                Mark as done
                                            </Button> : <div>Project is done</div>}
                                        </CardActions>
                                    </Card></Grid>))}
                        </Grid>
                    </Grid>
                </Grid>}
                {!this.state.loadProject ? null : <Project projectName={projectToLoad} stories={selectedProjStories} projects={this.state.projects} users={this.state.users}/>}
                {!this.state.addProject ? null : <AddProject open={true} openFromDialog={this.msgFromChild} users={this.state.users}/>}
            </MuiThemeProvider>
        )
    }
}

export default ProjectList;