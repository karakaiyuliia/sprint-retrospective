import React, { Component } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";
import Login from "./LoginComponent";
import Register from "./RegisterComponent";
import Dashboard from "./NavigationComponent";
import EstimateAccuracy from "./SharedComponents/EstimateAccuracy";
import "../App.css";
import LoginController from "../controllers/Login";
import ProjectController from "../controllers/Project";

class App extends Component {
  state = {
    authUser: null,
    isLoginOpen: true,
    isRegisterOpen: false,
    loggedIn: false,
    user: null,
    value: "",
    showEstimate: true
  };

  componentDidMount() {
    this.listener = LoginController.listenToAuth(user => {
      user
        ? this.setState({ authUser: user })
        : this.setState({ authUser: null });
    });
  }

  componentWillUnmount() {
    this.listener();
  }

  showRegister() {
    this.setState({ isRegisterOpen: true, isLoginOpen: false });
  }

  msgFromChild = msg => {
    this.setState({
      loggedIn: true,
      user: msg,
      isLoginOpen: false,
      isRegisterOpen: false
    });
  };

  showLogin() {
    this.setState({
      isLoginOpen: true,
      isRegisterOpen: false,
      showEstimate: false
    });
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        {!this.state.loggedIn && (
          <div className="root-container">
            <div className="box-controller">
              <div
                className={
                  "controller " +
                  (this.state.isLoginOpen ? "selected-controller" : "")
                }
                onClick={this.showLogin.bind(this)}
              >
                Login
              </div>
              <div
                className={
                  "controller " +
                  (this.state.isRegisterOpen ? "selected-controller" : "")
                }
                onClick={this.showRegister.bind(this)}
              >
                Register
              </div>
            </div>
            {this.state.isLoginOpen && (
              <Login dataFromChild={this.msgFromChild} />
            )}
            {this.state.isRegisterOpen && <Register />}
          </div>
        )}
        {this.state.loggedIn &&
          !this.state.isLoginOpen &&
          !this.state.isRegisterOpen && <Dashboard user={this.state.user}/>}
      </MuiThemeProvider>
    );
  }
}

export default App;
