import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import theme from "../theme";
import ComboSelect from "react-combo-select";
import "../../App.css";
require("../../../node_modules/react-combo-select/style.css");
let stories = []; // stories JSONs to be passed by parent

class EstimateAccuracy extends React.PureComponent {
  state = {
    selectedMsg: "",
    stories: [], // to be passed from parent component
    completedStoris: [],
    members: [], // to populated with a fetch from the database
    membername: "",
    estimationMsg: "Choose a member"
  };

  sendParentSomeData = msg => {
    this.props.dataFromChild(msg);
  };

  async componentDidMount() {
    stories = this.props.allStories;
    let membTemp = [];

    this.setState({
      selectedMsg: "",
      userInfo: "",
      stories: this.props.allStories
    });

    stories.map(s => {
      if (s.owner) if (!membTemp.includes(s.owner)) membTemp.push(s.owner);
    });

    this.setState({
      members: membTemp
    });
  }

  async onMemberChoosen(value, text) {
    // Fetch all user stories estimated by the particular member.
    let allUserStories = [];

    stories.map(s => {
      if (s.owner === text) allUserStories.push(s);
    });

    this.setState({
      estimationMsg: `user has ${allUserStories.length} stories`
    });

    let estimate = 0;
    let numEstimates = 0;
    allUserStories.map(story => {
      // Check whether the user story is completed or not.
      if (story.isDone) {
        numEstimates++;
        // If completed and number of hours worked is equal to the estimate, then percentage is 100%
        if (story.hoursEstimated === story.hoursSpent) estimate += 100;
        // If completed but number of hours worked is less than estimate, use: ( Number of hours worked รท estimate ) X -100 ( Where negative value means it is over-estimated)
        else if (story.hoursEstimated > story.hoursSpent)
          estimate += (story.hoursSpent / story.hoursEstimated) * -100;
        // If completed but number of hours worked is greater than the estimate, use: 100 - ( (100 X Number of hours worked รท estimate) - 100)
        else if (story.hoursEstimated < story.hoursSpent)
          estimate +=
            100 - ((100 * story.hoursSpent) / story.hoursEstimated - 100);
      }
    });

    let msg = `member ${text} has `;

    if (numEstimates === 0)
      msg +=
        "no completed user stories. Can't calculate estimate accuracy yet!";
    else
      estimate / numEstimates < 0
        ? (msg += `over-estimate accuracy of  ${(-1 * estimate) /
            numEstimates}%`)
        : (msg += `estimate accuracy of  ${estimate / numEstimates}%`);

    this.setState({
      estimationMsg: msg
    });
  }

  render() {
    const { members } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <Typography
            style={{ textAlign: "center" }}
            variant="h4"
            color="primary"
          >
            Member Estimate Accuracy
          </Typography>
        </div>

        <Card style={{ marginTop: "10%" }}>
          <CardHeader />

          <CardContent style={{ marginTop: "10%" }}>
            {/* Display ComboSelect containing members to choose form - using member array  */}
            {/* When user select a member, calculate estimate accuracy using stories JSON array */}
            <ComboSelect
              data={members}
              text="-Select a member-"
              onChange={this.onMemberChoosen.bind(this)}
            />

            {/** Display the estimate as plain text */}
            {this.state.estimationMsg}
          </CardContent>
        </Card>
      </MuiThemeProvider>
    );
  }
}

export default EstimateAccuracy;
