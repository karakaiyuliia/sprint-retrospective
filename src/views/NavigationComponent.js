import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Toolbar,
  Card,
  AppBar,
  CardHeader,
  CardContent,
  Menu,
  MenuItem,
  IconButton,
  Typography
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Reorder from "@material-ui/icons/Reorder";
import Project from "./ProjectListComponent";
import controller from "../controllers/Project";
import loginController from "../controllers/Login";
import membersController from "../controllers/Members";
import EstimateAccuracy from "./SharedComponents/EstimateAccuracy";
import HoursWorked from "./MemberComponents/HoursWorked";
import Backlog from "./BacklogComponent";
import SprintSummary from "./PMOcomponents/SprintSummary";
import ProjectMembers from "./ProjectMembersComponent";

class NavigationComponent extends React.PureComponent {
  state = {
    anchorEl: null,
    btn: "",
    projects: [],
    stories: [],
    sprints: [],
    projectComp: false,
    dashboard: true,
    showEstimates: false,
    showHoursWorked: false,
    showSprintSummary: false,
    showBacklog: false,
    showMembers: false,
    user: null,
    email: "",
    users: [],
    members: []
  };

  setAllFalse = () => {
    this.setState({
      projectComp: false,
      dashboard: false,
      showEstimates: false,
      showHoursWorked: false,
      showSprintSummary: false,
      showMembers: false,
      showBacklog: false
    });
  };

  async componentDidMount() {
    this.setState({ stories: await controller.getStories() });
    this.setState({ users: await loginController.getUsers() });
    this.setState({ projects: await controller.getProjects() });
    this.setState({ sprints: await controller.getSprints() });
    this.setState({ user: this.props.user });
    this.setState({ members: await membersController.getMembers() });

    for (let key in this.state.user) {
      if (this.state.user.hasOwnProperty(key))
        if (key === "user") {
          this.setState({ email: this.state.user[key].email });
        }
    }
  }

  msgFromProject = () => {
    this.setState({ projectComp: false, anchorEl: null });
  };

  onMenuItemClicked = async event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  onClose = () => {
    this.setState({ anchorEl: null });
  };
  onProjectClicked = () => {
    this.setAllFalse();
    this.setState({ projectComp: true, anchorEl: null });
  };
  onBacklogClicked = () => {
    this.setAllFalse();
    this.setState({ showBacklog: true, anchorEl: null });
  };
  onEstimatesClicked = () => {
    this.setState({ btn: "Relatives Estimates clicked", anchorEl: null });
    this.setAllFalse();
    this.setState({ showEstimates: true });
  };

  onHoursClicked = () => {
    this.setState({ btn: "Hours Worked clicked", anchorEl: null });
    this.setAllFalse();
    this.setState({ showHoursWorked: true });
  };

  onSummaryClicked = () => {
    this.setState({ btn: "Sprint Summary clicked", anchorEl: null });
    this.setAllFalse();
    this.setState({ showSprintSummary: true });
  };

  onMembersClicked = () => {
    this.setAllFalse();
    this.setState({ showMembers: true });
  };
  render() {
    const { anchorEl, btn } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit">
              <Reorder />
            </IconButton>
            <Typography
              variant="inherit"
              color="inherit"
              style={{ flexGrow: 1 }}
            >
              {this.state.email}
            </Typography>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem onClick={this.onProjectClicked}>Projects</MenuItem>
              <MenuItem onClick={this.onBacklogClicked}>
                Product Backlog
              </MenuItem>
              <MenuItem onClick={this.onEstimatesClicked}>
                Relatives Estimates
              </MenuItem>

              <MenuItem onClick={this.onHoursClicked}>Hours Worked</MenuItem>

              <MenuItem onClick={this.onSummaryClicked}>
                Sprint Summary
              </MenuItem>
              <MenuItem onClick={this.onMembersClicked}>Members</MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
        {!this.state.dashboard ? null : (
          <Card style={{ marginTop: "10%" }}>
            <CardHeader
              title="Sprint Retrospective"
              color="inherit"
              style={{ textAlign: "center" }}
            />
            <CardContent>
              {btn.length > 0 && <Typography color="error">{btn}</Typography>}
            </CardContent>
          </Card>
        )}
        {!this.state.projectComp ? null : (
          <Project
            stories={this.state.stories}
            projects={this.state.projects}
            users={this.state.users}
            projectList={this.msgFromProject}
          />
        )}
        {!this.state.showBacklog ? null : (
          <Backlog
            stories={this.state.stories}
            projects={this.state.projects}
            users={this.state.users}
          />
        )}
        {!this.state.showEstimates ? null : (
          <EstimateAccuracy allStories={this.state.stories} />
        )}

        {!this.state.showHoursWorked ? null : (
          <HoursWorked
            allStories={this.state.stories}
            allSprints={this.state.sprints}
          />
        )}

        {!this.state.showSprintSummary ? null : (
          <SprintSummary
            allStories={this.state.stories}
            allSprints={this.state.sprints}
          />
        )}
        {!this.state.showMembers ? null : (
          <ProjectMembers
            members={this.state.members}
            projects={this.state.projects}
          />
        )}
      </MuiThemeProvider>
    );
  }
}
export default NavigationComponent;
