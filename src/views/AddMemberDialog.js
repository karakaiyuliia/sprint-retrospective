import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import MenuItem from "@material-ui/core/MenuItem";
import controller from "../controllers/Members";

const users = [
  {
    value: "Abdullah A."
  },
  {
    value: "Marina P."
  },
  {
    value: "Yullia K."
  }
];

const projects = [
  {
    value: "Sprint Retrospective"
  },
  {
    value: "iOS Client"
  },
  {
    value: "Todo App"
  },
  {
    value: "YouTube"
  },
  {
    value: "Maps"
  },
  {
    value: "Other"
  }
];

export default class AddMember extends React.Component {
  state = {
    open: false,
    val: "",
    user: "",
    project: "",
    name: "",
    start: null,
    end: null,
    sprint: 0
  };

  async componentDidMount() {
    this.setState({ open: this.props.open });
  }

  sendParentSomeData = () => {
    this.props.openFromDialog(false);
  };

  handleDevChange = val => event => {
    this.setState({ [val]: event.target.value });
  };

  handleNameChange = e => {
    this.setState({ name: e.target.value });
  };

  handleStartChange = e => {
    this.setState({ start: e.target.value });
  };

  handleEndChange = e => {
    this.setState({ end: e.target.value });
  };

  handleSprintChange = e => {
    this.setState({ sprint: e.target.value });
  };

  handleClose = () => {
    this.setState({ open: false });

    this.sendParentSomeData();
  };

  handleSave = () => {
    console.log(`${this.state.user} and ${this.state.project}`);
    controller.addMember(this.state.user, this.state.project);
    this.handleClose();
  };

  render() {
    const emptyorundefined =
      this.state.user === undefined ||
      this.state.user.length === 0 ||
      this.state.project === undefined ||
      this.state.project === 0;

    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            Assign Member to Project
          </DialogTitle>
          <DialogContent>
            <TextField
              fullWidth
              required
              autoFocus
              margin="dense"
              id="dev"
              select
              label="Projects"
              value={this.state.project}
              onChange={this.handleDevChange("project")}
              SelectProps={{
                multiple: false,
                MenuProps: {}
              }}
            >
              {projects.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.value}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              fullWidth
              required
              autoFocus
              margin="dense"
              id="dev"
              select
              label="Member"
              value={this.state.user}
              onChange={this.handleDevChange("user")}
              SelectProps={{
                multiple: false,
                MenuProps: {}
              }}
            >
              {users.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.value}
                </MenuItem>
              ))}
            </TextField>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary">
              Cancel
            </Button>
            <Button
              onClick={this.handleSave}
              color="primary"
              disabled={emptyorundefined}
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
