import React from 'react';
import {MuiThemeProvider} from '@material-ui/core/styles';
import theme from "./theme";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import AddCircle from '@material-ui/icons/AddCircleRounded';
import AddStory from './AddStoryDialog';
import controller from "../controllers/Project";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import SaveIcon from '@material-ui/icons/Save';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

let name;

class Project extends React.Component{

    state={
        stories: [],
        project: null,
        checked: [0],
        addStory: false,
        users: this.props.users,
        projects: this.props.projects,
        projectId: [],
        finished: false,
        start: null,
        end: null,
        val: "",
        user: [],
        velocity: 0

    };

    async componentDidMount() {
        name = this.props.projectName;
        this.setState({stories: this.props.stories});
        this.setState({projectId: await controller.getId("projects", "name", name)});

        this.getProject();
        this.sendParentSomeData();
    }

    sendParentSomeData = () => {
        //this.props.projectList(false);

    };

    getProject(){
        this.state.projects.map((proj) => {
            if(proj.name === name){
                this.setState({project: proj});
                this.setState({start: proj.start});
                this.setState({end: proj.end});

                if(proj.finished === true){
                    this.setState({finished: true});
                }
            }
        });
    }

    onAddClicked = () => {
        this.setState({addStory: true})
    };

    msgFromChild = async(msg) => {
        this.setState({addStory: msg});
    };

    handleFinishedChange =() => event => {
        this.setState({ finished: event.target.checked });
    };

    // handleDevChange = val => event => {
    //     this.setState({ [val]: event.target.value });
    // };


    handleStartChange = e => {
        this.setState({ start: e.target.value });

    };

    handleEndChange = e => {
        this.setState({ end: e.target.value });

    };

    handleVelocityChange = e => {
        this.setState({ velocity: e.target.value });

    };

    handleUpdate = () =>{
        controller.updateProject(this.state.projectId,name,this.state.start,this.state.end,this.state.finished, this.state.velocity, this.state.project.developers)
    };

    render(){

        return (
            <MuiThemeProvider theme={theme}>
                <Card style={{ marginTop: '3%', maxWidth: 800, marginLeft: 'auto', marginRight: 'auto'}}>
                    <IconButton
                        color="secondary"
                        style={{marginTop: 5, float: 'right'}}
                        onClick={this.onAddClicked}
                    >
                        <AddCircle fontSize="large"/>
                    </IconButton>
                    <CardHeader
                        title={name}
                        color="inherit"
                        style={{ textAlign: 'center' }}
                    />
                    <CardContent>
                        <List style={{width: '100%', maxWidth: 360, marginLeft: 'auto', marginRight: 'auto'}}>
                            {this.state.stories.map((story, index) => (
                                <ListItem key={index} role={undefined} dense button >
                                    <ListItemText primary={story.name}/>
                                </ListItem>
                                ))}
                        </List>
                    </CardContent>
                </Card>
                <Card style={{ marginTop: '3%', maxWidth: 800, marginLeft: 'auto', marginRight: 'auto', marginBottom: '2%'}}>
                    <CardContent>
                        {this.state.project === null ? null : <div>
                        <TextField
                            fullWidth
                            required
                            type="number"
                            id="velocity"
                            label="Velocity"
                            defaultValue={this.state.project.velocity}
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleVelocityChange}
                        />
                        <TextField
                            fullWidth
                            required
                            type="date"
                            id="start"
                            label="Start date"
                            defaultValue={this.state.project.start}
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleStartChange}
                        />
                        <TextField
                            fullWidth
                            required
                            type="date"
                            id="end"
                            label="End date"
                            defaultValue={this.state.project.end}
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleEndChange}
                        />
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.state.finished}
                                        onChange={this.handleFinishedChange()}
                                        value="checkedA"
                                    />
                                }
                                label="Finished"
                            />
                            <TextField
                                fullWidth
                                required
                                autoFocus
                                margin="dense"
                                id="dev"
                                select
                                label="Developers"
                                value={this.state.project.developers[0]}
                                //onChange={this.handleDevChange('user')}
                                // SelectProps={{
                                //     multiple: true,
                                //     MenuProps: {
                                //     },
                                // }}
                            >
                                {this.state.project.developers.map(option => (
                                    <MenuItem key={option} value={option}>
                                        {option}
                                    </MenuItem>
                                    ))}
                            </TextField>
                        </div>
                        }
                        <Button color="primary" variant="contained" size="small" style={{float: 'right', marginTop: '2%', marginBottom: '2%'}} onClick={this.handleUpdate}>
                            <SaveIcon />
                            Save
                        </Button>
                    </CardContent>
                </Card>
                {!this.state.addStory ? null : <AddStory open={true} project={name} openFromDialog={this.msgFromChild} />}
            </MuiThemeProvider>
        )}
}
export default Project;