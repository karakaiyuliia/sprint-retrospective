import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from "@material-ui/core/MenuItem";
import controller from "../controllers/Project"

export default class AddProject extends React.Component {
    state = {
        open: false,
        val: "",
        user: [],
        name: "",
        start: "2019-03-01",
        end: "2019-05-01",
        velocity: 0,
        users: this.props.users
    };

    componentDidMount() {
        this.setState({open: this.props.open});
    };

    sendParentSomeData = () => {
        this.props.openFromDialog(false);

    };

    handleDevChange = val => event => {
        this.setState({ [val]: event.target.value });
    };

    handleNameChange = e => {
        this.setState({ name: e.target.value });

    };

    handleVelocityChange = e => {
        this.setState({ velocity: e.target.value });

    };

    handleStartChange = e => {
        this.setState({ start: e.target.value });

    };

    handleEndChange = e => {
        this.setState({ end: e.target.value });

    };

    handleClose = () => {
        this.setState({ open: false });

        this.sendParentSomeData();
    };

    handleSave = () => {
        controller.addProject(this.state.name, this.state.start, this.state.end, this.state.user, this.state.velocity);
        this.handleClose();
    };

    render() {

        const emptyorundefined =
            this.state.velocity === undefined ||
            this.state.user === undefined ||
            this.state.user.length === 0 ||
            this.state.name === undefined ||
            this.state.name === '' ||
            this.state.start === null ||
            this.state.start === '' ||
            this.state.end === null ||
            this.state.end === '';

        return (
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add new project</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="text"
                            onChange={this.handleNameChange}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="velocity"
                            label="Velocity"
                            type="number"
                            onChange={this.handleVelocityChange}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="start"
                            label="Start date"
                            type="date"
                            defaultValue="2019-03-01"
                            onChange={this.handleStartChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="end"
                            label="End date"
                            type="date"
                            defaultValue="2019-05-24"
                            onChange={this.handleEndChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                        <TextField
                            fullWidth
                            required
                            autoFocus
                            margin="dense"
                            id="dev"
                            select
                            label="Developers"
                            value={this.state.user}
                            onChange={this.handleDevChange('user')}
                            SelectProps={{
                                multiple: true,
                                MenuProps: {
                                },
                            }}
                        >
                            {this.state.users.map(option => (
                                <MenuItem key={option.email} value={option.email}>
                                    {option.email}
                                </MenuItem>
                            ))}
                        </TextField>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSave} color="primary" disabled={emptyorundefined}>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}