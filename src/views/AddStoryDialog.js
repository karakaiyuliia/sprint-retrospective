import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from "@material-ui/core/MenuItem";
import controller from "../controllers/Project"

const users = [
    {
        value: 'test@test.com',
    },
    {
        value: 'karakaiyuliia@gmail.com',
    },
    {
        value: 'abadi2007@gmail.com',
    },
];

export default class AddStory extends React.Component {
    state = {
        open: false,
        val: "",
        user: [],
        name: "",
        start: null,
        end: null,
        sprint: 0
    };

    async componentDidMount() {
        this.setState({open: this.props.open});
    };

    sendParentSomeData = () => {
        this.props.openFromDialog(false);

    };

    handleDevChange = val => event => {
        this.setState({ [val]: event.target.value });
    };


    handleNameChange = e => {
        this.setState({ name: e.target.value });

    };

    handleStartChange = e => {
        this.setState({ start: e.target.value });

    };

    handleEndChange = e => {
        this.setState({ end: e.target.value });

    };

    handleSprintChange = e => {
        this.setState({ sprint: e.target.value });

    };

    handleClose = () => {
        this.setState({ open: false });

        this.sendParentSomeData();
    };

    handleSave = () => {
        controller.addStory(this.state.name, this.state.start, this.state.end, this.state.user, this.props.project, this.state.sprint);
        this.handleClose();
    };

    render() {

        const emptyorundefined =
            this.state.user === undefined ||
            this.state.user.length === 0 ||
            this.state.name === undefined ||
            this.state.name === '' ||
            this.state.sprint === undefined ||
            this.state.sprint === 0 ||
            this.state.start === null ||
            this.state.start === '' ||
            this.state.end === null ||
            this.state.end === '' ||
            this.state.project === null ||
            this.state.project === '';

        return (
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add new project</DialogTitle>
                    <DialogContent>
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="text"
                            onChange={this.handleNameChange}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="start"
                            label="Start date"
                            type="date"
                            defaultValue="2019-03-01"
                            onChange={this.handleStartChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="end"
                            label="End date"
                            type="date"
                            defaultValue="2019-05-24"
                            onChange={this.handleEndChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="sprint"
                            label="Sprint"
                            type="number"
                            onChange={this.handleSprintChange}
                            fullWidth
                        />
                        <TextField
                            fullWidth
                            required
                            autoFocus
                            margin="dense"
                            id="dev"
                            select
                            label="Developers"
                            value={this.state.user}
                            onChange={this.handleDevChange('user')}
                            SelectProps={{
                                multiple: true,
                                MenuProps: {
                                },
                            }}
                        >
                            {users.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.value}
                                </MenuItem>
                            ))}
                        </TextField>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSave} color="primary" disabled={emptyorundefined}>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}