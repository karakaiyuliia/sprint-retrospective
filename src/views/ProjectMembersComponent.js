import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";
import Card from "@material-ui/core/Card";
import IconButton from "@material-ui/core/IconButton";
import AddCircle from "@material-ui/icons/AddCircleRounded";
import controller from "../controllers/Project";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import AddMember from "./AddMemberDialog";

let name;

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3,
    overflowX: "auto"
  },
  table: {
    minWidth: 700
  }
});

class ProjectMembers extends React.Component {
  state = {
    stories: [],
    project: null,
    checked: [0],
    addStory: false,
    users: this.props.users,
    projects: this.props.projects,
    projectId: [],
    finished: false,
    start: null,
    end: null,
    val: "",
    user: [],
    velocity: 0,
    members: this.props.members,
    addProject: false,
    addMember: false
  };

  async componentDidMount() {
    name = this.props.projectName;
    this.setState({ members: this.props.members });
    this.sendParentSomeData();

    this.setState({ stories: this.props.stories });

    this.getProject();
    this.sendParentSomeData();
  }

  sendParentSomeData = () => {
    //this.props.projectList(false);
  };

  getProject() {
    this.state.projects.map(proj => {
      if (proj.name === name) {
        this.setState({ project: proj });
        this.setState({ start: proj.start });
        this.setState({ end: proj.end });

        if (proj.finished === true) {
          this.setState({ finished: true });
        }
      }
    });
  }

  onAddClicked = () => {
    this.setState({ addStory: true });
  };

  msgFromChild = async msg => {
    this.setState({ addStory: msg });
  };

  handleFinishedChange = () => event => {
    this.setState({ finished: event.target.checked });
  };

  // handleDevChange = val => event => {
  //     this.setState({ [val]: event.target.value });
  // };

  handleStartChange = e => {
    this.setState({ start: e.target.value });
  };

  handleEndChange = e => {
    this.setState({ end: e.target.value });
  };

  handleVelocityChange = e => {
    this.setState({ velocity: e.target.value });
  };

  handleUpdate = () => {
    controller.updateProject(
      this.state.projectId,
      name,
      this.state.start,
      this.state.end,
      this.state.finished,
      this.state.velocity,
      this.state.project.developers
    );
  };

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Card
          style={{
            marginTop: "3%",
            maxWidth: 800,
            marginLeft: "auto",
            marginRight: "auto"
          }}
        >
          <IconButton
            color="secondary"
            style={{ marginTop: 5, float: "right" }}
            onClick={this.onAddClicked}
          >
            <AddCircle fontSize="large" />
          </IconButton>

          <Paper>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Project</TableCell>
                  <TableCell align="left">Members Assigned</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.members.map((row, index) => (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {row.projectName}
                    </TableCell>
                    <TableCell align="left">{row.memberName}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        </Card>
        <Card
          style={{
            marginTop: "3%",
            maxWidth: 800,
            marginLeft: "auto",
            marginRight: "auto",
            marginBottom: "2%"
          }}
        />
        {!this.state.addStory ? null : (
          <AddMember
            open={true}
            project={name}
            openFromDialog={this.msgFromChild}
            members={this.state.members}
            projects={this.state.projects}
          />
        )}
      </MuiThemeProvider>
    );
  }
}

export default ProjectMembers;
