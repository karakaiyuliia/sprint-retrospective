import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import {MuiThemeProvider} from "@material-ui/core";
import theme from "./theme";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Fab from "@material-ui/core/Fab";
import EditStory from "./EditStoryDialog";

class BackLog extends React.PureComponent{

    state= {
        projects: this.props.projects,
        stories: this.props.stories,
        projectStories: [],
        project: '',
        sprintsNum: 0,
        sprints: [],
        editStory: false,
        storyName: "",
        storySprint: "",
        storyHoursWorked: 0,
        storyReestimate: 0,
        storyOwner: "",
        storyStart: "",
        storyEnd: "",
        storyIsDone: false,
        storyProject: "",
        storyDevelopers: [],
    };

    componentDidMount() {
    }

    editStory = (name, sprint, hoursWorked, Reestimate, owner, start, end, isDone, project, developers) => {
        this.setState({storyName: name});
        this.setState({storySprint: sprint});
        this.setState({storyHoursWorked: hoursWorked});
        this.setState({storyReestimate: Reestimate});
        this.setState({storyOwner: owner});
        this.setState({storyStart: start});
        this.setState({storyEnd: end});
        this.setState({storyProject: project});
        this.setState({storyIsDone: isDone});
        this.setState({storyDevelopers: developers});
        this.setState({editStory: true})
    };

    finishStory = () => {
    };

    msgFromChild = async(msg) => {
        this.setState({editStory: msg});
    };

    handleProjChange = project => event => {
        this.setState({project: event.target.value},
            () => {this.setState({projectStories: this.getProjectStories()},
            () => {this.setState({sprints: this.getLastSprint()},
             ()=>{this.setState({sprintsNum:  this.getSprintNum()})
            })
            })
        });
    };

    getProjectStories = () =>{
        let projectStories = [];
        this.state.stories.map((story)=>{
          if(story.project === this.state.project)
              projectStories.push(story);
      });
      return projectStories;
    };

    getSprintNum = () =>{
        let currentHighest = this.state.sprints[0];
        for(let i = 0; i < this.state.sprints.length; ++i){
          if(this.state.sprints[i] > currentHighest){
              currentHighest = this.state.sprints[i];
          }
      }

      return currentHighest;
    };

    getLastSprint = () => {
        let sprints = [];
        this.state.projectStories.map((story) =>{
            if(!sprints.includes(story.sprint))
                sprints.push(story.sprint);
        });

        return sprints;
    };

    render(){
        return(
            <MuiThemeProvider theme={theme}>
                <div style={{margin: '2%', maxWidth: '200px'}}>
                    <TextField
                        fullWidth
                        required
                        autoFocus
                        margin="dense"
                        id="prj"
                        select
                        label="Project"
                        value={this.state.project}
                        onChange={this.handleProjChange('project')}
                        SelectProps={{
                            MenuProps: {
                                width: 200
                            },
                        }}
                    >
                        {this.state.projects.map(option => (
                            <MenuItem key={option.name} value={option.name}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                {this.state.sprintsNum === 0 ? null :
                    <div>
                        {this.state.sprints.map((sprint, index)=>(
                            <Card key={index} style={{margin: '2%', maxWidth: 800, marginLeft: 'auto', marginRight: 'auto'}}>
                                <CardHeader
                                    title={`Sprint ${index+1}`}
                                    color="inherit"
                                    style={{ textAlign: 'center' }}
                                />
                                <CardContent>
                                    {this.state.projectStories.map((story, index1) => (
                                    <List key={index1} style={{width: '100%', maxWidth: 800, marginLeft: 'auto', marginRight: 'auto'}}>
                                            {story.sprint === index+1 ? (
                                                    <ListItem key={index} role={undefined} dense button >
                                                        <ListItemText primary={story.name} style={{fontSize: 16}}/>
                                                        <ListItemSecondaryAction style={{marginRight: '10px'}}>
                                                            {!story.isDone ?
                                                                <Fab
                                                                variant="extended"
                                                                size="small"
                                                                color="primary"
                                                                aria-label="Finish"
                                                                onClick={() => this.finishStory()}
                                                            >
                                                                Finish
                                                                </Fab> :
                                                                <Fab
                                                                    disabled
                                                                    variant="extended"
                                                                    size="small"
                                                                    color="primary"
                                                                    aria-label="Finish"
                                                                    onClick={() => this.finishStory()}
                                                                >
                                                                    Finished
                                                                </Fab>}
                                                            {!story.isDone ?
                                                            <Fab
                                                                variant="extended"
                                                                size="small"
                                                                color="secondary"
                                                                aria-label="Reestimate"
                                                                onClick={() => this.editStory(story.name, story.sprint,
                                                                    story.hoursWorked,
                                                                    story.Reestimate,
                                                                    story.owner,
                                                                    story.start,
                                                                    story.end,
                                                                    story.isDone,
                                                                    story.project,
                                                                    story.developers)}
                                                            >
                                                                Reestimate
                                                            </Fab>:
                                                                <Fab
                                                                    disabled
                                                                    variant="extended"
                                                                    size="small"
                                                                    color="secondary"
                                                                    aria-label="Reestimate"
                                                                    onClick={() => this.editStory(story.name, story.sprint,
                                                                        story.hoursWorked,
                                                                        story.Reestimate,
                                                                        story.owner,
                                                                        story.start,
                                                                        story.end,
                                                                        story.isDone,
                                                                        story.project,
                                                                        story.developers)}
                                                                >
                                                                    Reestimate
                                                                </Fab>}
                                                        </ListItemSecondaryAction>
                                                    </ListItem>
                                                ) : null}
                                    </List>
                                        ))}
                                </CardContent>
                            </Card>
                        ))}
                    </div>
                }
                {!this.state.editStory ? null : <EditStory open={true} story={this.state.storyName} project={this.state.storyProject}
                                                           sprint={this.state.storySprint} hoursWorked={this.state.storyHoursWorked}
                                                           Reestimate={this.state.storyReestimate} owner={this.state.storyOwner}
                                                           start={this.state.storyStart} end={this.state.storyEnd} isDone={this.state.storyIsDone}
                                                           developers={this.state.storyDevelopers} openFromDialog={this.msgFromChild} />}
            </MuiThemeProvider>
        )
    }

}
export default BackLog;
