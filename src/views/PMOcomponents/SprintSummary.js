import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActionArea,
  Grid
} from "@material-ui/core";
import theme from "../theme";
import ComboSelect from "react-combo-select";

import "../../App.css";
require("../../../node_modules/react-combo-select/style.css");
let stories = []; // stories JSONs to be passed by parent

class SprintSummary extends React.PureComponent {
  state = {
    stories: [], // to be passed from parent component
    SprintStories: [],
    NumberOfHours: [],
    members: [], // to populated with a fetch from the database
    chosenStory: ""
  };

  sendParentSomeData = msg => {
    this.props.dataFromChild(msg);
  };

  async componentDidMount() {
    stories = this.props.allStories;

    this.setState({
      userInfo: "",
      stories: this.props.allStories,
      members: this.props.allSprints
    });
  }

  async onSprintChoosen(value, text) {
    // Fetch all user stories in that sprint.
    let allUserStories = [];

    stories.map(s => {
      if (s.sprint === value) allUserStories.push(s);
    });

    this.setState({ SprintStories: allUserStories, chosenStory: value });
  }

  render() {
    const { members } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <div>
          <Typography
            style={{ textAlign: "center" }}
            variant="h4"
            color="primary"
          >
            A summary report for sprints
          </Typography>
        </div>

        <Card style={{ marginTop: "10%" }}>
          <CardHeader />

          <CardContent style={{ marginTop: "10%" }}>
            <div style={{ width: "30%" }}>
              <ComboSelect
                autosize={false}
                data={members}
                map={{ text: "sprint", value: "sprint" }}
                text="-Select a sprint-"
                onChange={this.onSprintChoosen.bind(this)}
              />
            </div>

            {/** Display the estimate as plain text */}
            <h3>Display summary report for sprint #{this.state.chosenStory}</h3>
            {this.state.SprintStories.map((story, index) => (
              <Grid key={index} item>
                <Card
                  style={{
                    margin: 5,
                    float: "left",
                    maxWidth: "350px",
                    minWidth: "300px",
                    alignContent: "center"
                  }}
                  key={index}
                >
                  <CardActionArea>
                    <CardContent>
                      <Typography gutterBottom variant="h6" component="h2">
                        {story.name}
                      </Typography>

                      <Typography
                        style={{ textAlign: "left" }}
                        variant="h6"
                        color="primary"
                      >
                        {story.isDone ? (
                          <p>status: Story is completed</p>
                        ) : (
                          <p>status: Story is not completed</p>
                        )}

                        <p>Time spent: {story.hoursSpent}</p>

                        {story.Reestimate > 0 ? (
                          <p>Re-estimate: {story.Reestimate}</p>
                        ) : (
                          <p>Re-estimate: Story has no Re-estimates</p>
                        )}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </CardContent>
        </Card>

        <br />
        <br />
        <br />
      </MuiThemeProvider>
    );
  }
}

export default SprintSummary;
