import model from "../models/database";

function addProject(name, start, end, users, velocity) {
  let data = {
    name: name,
    start: start,
    end: end,
    finished: false,
    developers: users,
    velocity: velocity
  };

  model.pushData("projects", data);
}

function addStory(name, start, end, users, project, sprint) {
  let data = {
    Reestimate: 0,
    hoursWorked: 0,
    owner: "test@test.com",
    sprint: sprint,
    name: name,
    start: start,
    end: end,
    isDone: false,
    project: project,
    developers: users
  };

  model.pushData("stories", data);
}

function updateProject(id, name, start, end, finished, velocity, users) {
  let data = {
    uid: id,
    name: name,
    start: start,
    end: end,
    finished: finished,
    velocity: velocity,
    developers: users,
  };

  model.updateDocument("projects", data);
}

async function updateStory(id, sprint, worked, estimate, owner, name, start, end, isDone, project, developers) {
    let data = {
        uid: id,
        sprint: sprint,
        hoursWorked: worked,
        Reestimate: estimate,
        owner: owner,
        name: name,
        start: start,
        end: end,
        isDone: isDone,
        project: project,
        developers: developers
    };

    model.updateDocument("stories", data);
}

async function getProjects() {
  return await model.getAllData("projects");
}

async function getStories() {
  return await model.getAllData("stories");
}

async function getSprints() {
  return await model.getAllData("sprints");
}

async function getId(coll, field, value) {
  return await model.getId(coll, field, value);
}

export default {
  addProject,
  addStory,
  getProjects,
  getStories,
  getSprints,
  getId,
  updateProject,
  updateStory
};
