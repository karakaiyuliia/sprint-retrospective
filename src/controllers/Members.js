import model from "../models/database";

async function getMembers() {
  return await model.getAllData("projectMembers");
}

function addMember(memberName, projectName) {
  let data = {
    projectName: projectName,
    memberName: memberName
  };

  model.pushData("projectMembers", data);
}

export default {
  addMember,
  getMembers
};
