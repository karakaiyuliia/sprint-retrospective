import firebase from "firebase";

// Initialize Firebase
let config = {
    apiKey: "AIzaSyCHhLfl3E5cokm0QtDFRaeinbuHhIkT18Y",
    authDomain: "sprint-retrospective-6265a.firebaseapp.com",
    projectId: "sprint-retrospective-6265a",
    databaseURL: "https://sprint-retrospective-6265a.firebaseio.com",
};
firebase.initializeApp(config);


//register user
const signUp = (email, password) =>
    firebase.auth().createUserWithEmailAndPassword(email, password);

//log in
const signIn = (email, password) =>
    firebase.auth().signInWithEmailAndPassword(email, password);

const statusChanged = () =>
    firebase.auth().onAuthStateChanged((user) => {
        return user;
    });

const pushData = (collection, data) =>{
    firebase.firestore().collection(collection).add(data).then(() => alert("Successfully added")).catch(() => alert("Error adding"));
};

const getAllData = (collection) =>{
    let documents = [];
    firebase.firestore().collection(collection).get()
        .then(snapshot =>{
            snapshot.forEach(doc => {
                documents.push(doc.data());
            })
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

    return documents;
};

const getId = (collection, field, value) => {
    let id = [];

    firebase.firestore().collection(collection).where(field, '==', value).get()
        .then(snapshot => {
            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }

            snapshot.forEach(doc => {
                id.push(doc.id);
            });
        });

    return id;
};

const updateDocument = (collection, data) => {
    let id = data.uid.toString();
    delete data.uid;
    firebase.firestore().collection(collection).doc(id).set(data)
        .then(()=>alert("Updated!"))
        .catch(error => alert(error));
};


export default {
    signUp,
    signIn,
    statusChanged,
    pushData,
    getAllData,
    updateDocument,
    getId
}




